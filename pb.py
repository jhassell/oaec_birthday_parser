import datetime
import csv
import re
import json,httplib

from bs4 import BeautifulSoup
soup = BeautifulSoup(open("hs.html"), 'html.parser')
hand = open('hs.html')
expiration_interval = 420
daily_push_time = "09:00"

f = open('bstring.csv','w+')
f.write('Push Date,Push Time,Push Text,Push Expiration Interval (seconds)\r\n')
for line in hand:
    line = line.rstrip()
    x = re.findall('450.*colspan=\"3\"\>(.*) \(',line)
    if len(x) > 0 : 
        name = x[0]
        name = " ".join(name.split())

    #if re.search('450', line) :
    #    print line
    x = re.findall('td.*([0-9]{2}/[0-9]{2})/[0-9]{4}', line)
    if len(x) > 0 : 
        date = x[0]
        newline = "%s/16, %s, Happy Birthday to State %s!, %s\r\n" % (date, daily_push_time, name, expiration_interval)
	# print newline,
	f.write(newline)

f.close()

connection = httplib.HTTPSConnection('api.parse.com', 443)
connection.connect()
with open('bstring.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    today = datetime.date.today()
    margin = datetime.timedelta(days=7)
    for row in reader:
        rowDate = datetime.datetime.strptime(row['Push Date'], '%m/%d/%y').date() 
        if ((rowDate > today) and (rowDate <= (today + margin))):
            dateInParseFormat = datetime.datetime.strptime(row['Push Date'], '%m/%d/%y').strftime('%Y-%m-%d').strip()
            timeInParseFormat = datetime.datetime.strptime(row['Push Time'].strip(), '%H:%M').strftime('%H:%M:00').strip()
            pushTime = "%sT%s" % (dateInParseFormat,timeInParseFormat)
            pushAlert = row['Push Text']
            print(pushTime)
            print(pushAlert)

            connection.request('POST', '/1/push', json.dumps({
            	"where":{},
            	"push_time": pushTime,
                    "expiration_interval":420,
                   	"data": {
                     "alert": pushAlert
                   }
                 }), {
                   "X-Parse-Application-Id": "CAyQsLynTJcl5D93CGysLwghBdDnawaOnn51tdgy",
                   "X-Parse-REST-API-Key": "41N2CZoM6JhDQnLKgBNzZb28lDtcoUa9X3MDGUNs",
                   "Content-Type": "application/json"
                 })
            result = json.loads(connection.getresponse().read())
            print result

#connection.request('POST', '/1/push', json.dumps({
#	"where":{},
#	"push_time": "2015-12-30T09:00:00",
#        "expiration_interval":420,
#       	"data": {
#         "alert": "State Rep. Karen Smith's birthday is today."
#       }
#     }), {
#       "X-Parse-Application-Id": "CAyQsLynTJcl5D93CGysLwghBdDnawaOnn51tdgy",
#       "X-Parse-REST-API-Key": "41N2CZoM6JhDQnLKgBNzZb28lDtcoUa9X3MDGUNs",
#       "Content-Type": "application/json"
#     })
#result = json.loads(connection.getresponse().read())
#print result
#
#
#
#connection.request('POST', '/1/push', json.dumps({
#	"where":{},
#	"push_time": "2015-12-31T09:00:00",
#        "expiration_interval":420,
#       	"data": {
#         "alert": "State Sen. Paul Smith's birthday is today."
#       }
#     }), {
#       "X-Parse-Application-Id": "CAyQsLynTJcl5D93CGysLwghBdDnawaOnn51tdgy",
#       "X-Parse-REST-API-Key": "41N2CZoM6JhDQnLKgBNzZb28lDtcoUa9X3MDGUNs",
#       "Content-Type": "application/json"
#     })
#result = json.loads(connection.getresponse().read())
#print result
#
#
#
#
#connection.request('POST', '/1/push', json.dumps({
#	"where":{},
#	"push_time": "2016-01-01T09:00:00",
#        "expiration_interval":420,
#       	"data": {
#         "alert": "State Sen. Gene Smith's birthday is today."
#       }
#     }), {
#       "X-Parse-Application-Id": "CAyQsLynTJcl5D93CGysLwghBdDnawaOnn51tdgy",
#       "X-Parse-REST-API-Key": "41N2CZoM6JhDQnLKgBNzZb28lDtcoUa9X3MDGUNs",
#       "Content-Type": "application/json"
#     })
#result = json.loads(connection.getresponse().read())
#print result
#
#
#
#
#connection.request('POST', '/1/push', json.dumps({
#	"where":{},
#	"push_time": "2016-01-02T09:00:00",
#        "expiration_interval":420,
#       	"data": {
#         "alert": "State Sen. Tom Smith's birthday is today."
#       }
#     }), {
#       "X-Parse-Application-Id": "CAyQsLynTJcl5D93CGysLwghBdDnawaOnn51tdgy",
#       "X-Parse-REST-API-Key": "41N2CZoM6JhDQnLKgBNzZb28lDtcoUa9X3MDGUNs",
#       "Content-Type": "application/json"
#     })
#result = json.loads(connection.getresponse().read())
#print result
#
#
#
#
#connection.request('POST', '/1/push', json.dumps({
#	"where":{},
#	"push_time": "2016-01-03T09:00:00",
#        "expiration_interval":420,
#       	"data": {
#         "alert": "State Sen. Gary Smith's birthday is today."
#       }
#     }), {
#       "X-Parse-Application-Id": "CAyQsLynTJcl5D93CGysLwghBdDnawaOnn51tdgy",
#       "X-Parse-REST-API-Key": "41N2CZoM6JhDQnLKgBNzZb28lDtcoUa9X3MDGUNs",
#       "Content-Type": "application/json"
#     })
#result = json.loads(connection.getresponse().read())
#print result
#
#
#
#
#
#connection.request('POST', '/1/push', json.dumps({
#	"where":{},
#	"push_time": "2016-01-04T09:00:00",
#        "expiration_interval":420,
#       	"data": {
#         "alert": "State Sen. Treavor Smith's birthday is today."
#       }
#     }), {
#       "X-Parse-Application-Id": "CAyQsLynTJcl5D93CGysLwghBdDnawaOnn51tdgy",
#       "X-Parse-REST-API-Key": "41N2CZoM6JhDQnLKgBNzZb28lDtcoUa9X3MDGUNs",
#       "Content-Type": "application/json"
#     })
#result = json.loads(connection.getresponse().read())
#print result
#
#
#
#connection.request('POST', '/1/push', json.dumps({
#	"where":{},
#	"push_time": "2016-01-05T09:00:00",
#        "expiration_interval":420,
#       	"data": {
#         "alert": "State Sen. Jill Smith's birthday is today."
#       }
#     }), {
#       "X-Parse-Application-Id": "CAyQsLynTJcl5D93CGysLwghBdDnawaOnn51tdgy",
#       "X-Parse-REST-API-Key": "41N2CZoM6JhDQnLKgBNzZb28lDtcoUa9X3MDGUNs",
#       "Content-Type": "application/json"
#     })
#result = json.loads(connection.getresponse().read())
#print result
#
